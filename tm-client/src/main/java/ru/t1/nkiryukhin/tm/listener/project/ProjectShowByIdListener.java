package ru.t1.nkiryukhin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nkiryukhin.tm.dto.event.ConsoleEvent;
import ru.t1.nkiryukhin.tm.dto.model.ProjectDTO;
import ru.t1.nkiryukhin.tm.dto.request.ProjectGetByIdRequest;
import ru.t1.nkiryukhin.tm.dto.response.ProjectGetByIdResponse;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken());
        request.setId(id);
        @NotNull ProjectGetByIdResponse response = projectEndpoint.getProjectById(request);
        @Nullable final ProjectDTO project = response.getProject();
        showProject(project);
    }

}
