package ru.t1.nkiryukhin.tm.service;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.nkiryukhin.tm.api.service.IConnectionService;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@Service
public class ConnectionService implements IConnectionService {

    @NotNull
    @Override
    public Liquibase getLiquibase() throws IOException, SQLException, DatabaseException {
        final ClassLoaderResourceAccessor accessor = new ClassLoaderResourceAccessor();
        final Properties properties = new Properties();
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        final Connection connection = DriverManager.getConnection(properties.getProperty("url"),
                properties.getProperty("username"), properties.getProperty("password"));
        final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        final String changeLogFile = properties.getProperty("changeLogFile");
        return new Liquibase(changeLogFile, accessor, database);
    }

}