package ru.t1.nkiryukhin.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nkiryukhin.tm.api.service.model.ITaskService;
import ru.t1.nkiryukhin.tm.enumerated.CustomSorting;
import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.nkiryukhin.tm.exception.entity.TaskNotFoundException;
import ru.t1.nkiryukhin.tm.exception.field.*;
import ru.t1.nkiryukhin.tm.exception.user.UserNotFoundException;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.model.Task;
import ru.t1.nkiryukhin.tm.model.User;
import ru.t1.nkiryukhin.tm.repository.model.ProjectRepository;
import ru.t1.nkiryukhin.tm.repository.model.TaskRepository;

import java.util.Collections;
import java.util.List;

@Service
public final class TaskService extends AbstractUserOwnedService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private TaskRepository repository;

    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @NotNull
    @Override
    protected TaskRepository getRepository() {
        return repository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().deleteByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task(name, description);
        User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        task.setUser(user);
        repository.save(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findByUserIdAndProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findByUserId(userId);
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable final CustomSorting sorting) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sorting == null) return findAll(userId);
        final Sort sort = Sort.by(Sort.Direction.ASC, getSortType(sorting.getComparator()));
        return getRepository().findByUserId(userId, sort);
    }

    @Nullable
    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) throws AbstractFieldException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findByUserIdAndId(userId, id).orElse(null);
    }

    @Override
    public int getSize(@Nullable final String userId) throws UserIdEmptyException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return (int) getRepository().countByUserId(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateProjectId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        if (projectId == null) task.setProject(null);
        else
        {
            @Nullable Project project = projectRepository.findByUserIdAndId(userId, projectId).orElse(null);
            if (project == null) throw new ProjectNotFoundException();
            task.setProject(project);
        }
        repository.save(task);
    }

}
