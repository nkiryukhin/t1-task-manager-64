package ru.t1.nkiryukhin.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.nkiryukhin.tm")
public class ApplicationConfiguration {
}
